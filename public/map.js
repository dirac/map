// Create variable to hold map element, give initial settings to map
var map = L.map('dirac-map', {
    center: [25.0, 5.0],
    minZoom: 2,
    zoom: 2
});

// Add OpenStreetMap tile layer to map element
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a', 'b', 'c']
}).addTo(map);

L.control.scale().addTo(map);

// Create point feature for Institutions
markers = [{
        "city": "Aarhus, Denmark",
        "lat": 56.168186,
        "lng": 10.202947,
        "name": "University of Aarhus",
        "url": "https://international.au.dk"
    }, {
        "city": "Amsterdam, Netherlands",
        "lat": 52.333750,
        "lng": 4.865806,
        "name": "Vrije Universiteit Amsterdam",
        "url": "https://www.vu.nl"
    }, {
        "city": "Banska Bystrica, Slovakia",
        "lat": 48.733329,
        "lng": 19.145466,
        "name": "Matej Bel University",
        "url": "https://www.umb.sk"
    }, {
        "city": "Berkeley, USA",
        "lat": 37.871134,
        "lng": -122.27397,
        "name": "University of Berkeley",
        "url": "https://www.berkeley.edu/"
    }, {
        "city": "Bratislava, Slovakia",
        "lat": 48.148970,
        "lng": 17.103600,
        "name": "Slovak Academy of Sciences",
        "url": "https://www.sav.sk/?lang=en"
    }, {
        "city": "Brunswick, Germany",
        "lat": 52.274010,
        "lng": 10.529117,
        "name": "TU Braunschweig",
        "url": "https://www.tu-braunschweig.de"
    }, {
        "city": "Budapest, Hungary",
        "lat": 47.481486,
        "lng": 19.055816,
        "name": "BME - Budapest Univ. Tech. & Econ.",
        "url": "https://www.bme.hu"
    }, {
        "city": "Cardiff, UK",
        "lat": 51.482759,
        "lng": -3.165571,
        "name": "Cardiff University",
        "url": "https://www.cardiff.ac.uk/"
    }, {
        "city": "Copenhague, Denmark",
        "lat": 55.785538,
        "lng": 12.521284,
        "name": "Technical University of Denmark",
        "url": "https://www.dtu.dk"
    }, {
        "city": "Corrientes, Argentina",
        "lat": -27.466882,
        "lng": -58.783842,
        "name": "CONICET - Northeast National University",
        "url": "https://imit.conicet.gov.ar/?lan=en"
    }, {
        "city": "Daejeon, South Korea",
        "lat": 36.372143,
        "lng": 127.360369,
        "name": "KAIST",
        "url": "https://www.kaist.edu"
    }, {
        "city": "Detroit, USA",
        "lat": 42.277956,
        "lng": -83.738224,
        "name": "University of Michigan",
        "url": "https://umich.edu/"
    }, {
        "city": "Graz, Austria",
        "lat": 47.070739,
        "lng": 15.429274,
        "name": "Technical University Graz",
        "url": "https://www.tugraz.at/en/home/"
    }, {
        "city": "Groningen, Netherlands",
        "lat": 53.219206,
        "lng": 6.562901,
        "name": "University of Groningen",
        "url": "https://www.rug.nl/"
    }, {
        "city": "Heidelberg, Germany",
        "lat": 49.419161,
        "lng": 8.670206,
        "name": "University of Heidelberg",
        "url": "https://www.uni-heidelberg.de"
    }, {
        "city": "Karlsruhe, Germany",
        "lat": 49.011899,
        "lng": 8.416784,
        "name": "Karlsruhe Institute of Technology",
        "url": "https://www.kit.edu"
    }, {
        "city": "Kassel, Germany",
        "lat": 51.322853,
        "lng": 9.507224,
        "name": "University of Kassel",
        "url": "https://www.uni-kassel.de"
    }, {
        "city": "Kyoto, Japan",
        "lat": 35.085282,
        "lng": 135.772382,
        "name": "Kyoto University",
        "url": "https://www.kyoto-u.ac.jp/en"
    }, {
        "city": "Lille, France",
        "lat": 50.631833,
        "lng": 3.075783,
        "name": "CNRS - Universite de Lille",
        "url": "https://www.univ-lille.fr"
    }, {
        "city": "Linköping, Sweden",
        "lat": 58.398349,
        "lng": 15.580309,
        "name": "Linkoeping University",
        "url": "https://liu.se"
    }, {
        "city": "Lund, Sweden",
        "lat": 55.711997,
        "lng": 13.203493,
        "name": "Lund University",
        "url": "https://lunduniversity.lu.se"
    }, {
        "city": "Montpellier, France",
        "lat": 43.610483,
        "lng": 3.874508,
        "name": "CNRS/Universite de Montpellier",
        "url": "https://www.umontpellier.fr/en/"
    }, {
        "city": "Mülheim, Germany",
        "lat": 51.417066,
        "lng": 6.885835,
        "name": "Max Planck Institute for Coal Research",
        "url": "https://www.kofo.mpg.de"
    }, {
        "city": "Mumbai, India",
        "lat": 19.013463,
        "lng": 72.922060,
        "name": "Bhabha Atomic Research Centre",
        "url": "https://www.barc.gov.in"
    }, {
        "city": "Nagoya, Japan",
        "lat": 35.138506,
        "lng": 136.966159,
        "name": "Chukyo University",
        "url": "https://www.chukyo-u.ac.jp"
    }, {
        "city": "Odense, Denmark",
        "lat": 55.369159,
        "lng": 10.429052,
        "name": "University of Southern Denmark",
        "url": "https://www.sdu.dk"
    }, {
        "city": "Oslo, Norway",
        "lat": 59.939937,
        "lng": 10.721900,
        "name": "University of Oslo",
        "url": "https://www.uio.no"
    }, {
        "city": "Portland, USA",
        "lat": 45.542107,
        "lng": -122.688298,
        "name": "Schrodinger, Inc.",
        "url": "https://www.schrodinger.com"
    }, {
        "city": "Stockholm, Sweden",
        "lat": 59.349655,
        "lng": 18.070565,
        "name": "Stockholm Inst. of Technology",
        "url": "https://www.kth.se"
    }, {
        "city": "Stockolm, Sweden",
        "lat": 59.349655,
        "lng": 18.070565,
        "name": "EuroCC National Competence Centre",
        "url": "https://enccs.se/"
    }, {
        "city": "Strasbourg, France",
        "lat": 48.579147,
        "lng": 7.766530,
        "name": "University of Strasbourg",
        "url": "https://en.unistra.fr"
    }, {
        "city": "Tel Aviv, Israel",
        "lat": 32.113450,
        "lng": 34.804205,
        "name": "University of Tel Aviv",
        "url": "https://english.tau.ac.il"
    }, {
        "city": "Toulouse, France",
        "lat": 43.562210,
        "lng": 1.470193,
        "name": "Universite Toulouse III",
        "url": "https://www.univ-tlse3.fr"
    }, {
        "city": "Tromso, Norway",
        "lat": 69.679870,
        "lng": 18.970604,
        "name": "UiT The Arctic University of Norway",
        "url": "https://en.uit.no"
    }, {
        "city": "Tubingen, Germany",
        "lat": 48.524300,
        "lng": 9.052575,
        "name": "University of Tubingen",
        "url": "https://uni-tuebingen.de/en/"
    }, {
        "city": "Warsaw, Poland",
        "lat": 52.240294,
        "lng": 21.018805,
        "name": "University of Warsaw",
        "url": "https://en.uw.edu.pl"
    }, {
        "city": "Zurich, Switzerland",
        "lat": 47.376476,
        "lng": 8.548368,
        "name": "ETH Zuerich",
        "url": "https://ethz.ch"
    }

];

for (var i = 0; i < markers.length; ++i) {
    L.marker([markers[i].lat, markers[i].lng])
        .bindPopup('<a href="' + markers[i].url + '" target="_blank">' + markers[i].name + '</a><p>' + markers[i].city + '</p>')
        .addTo(map);
}

// Listen for a click event on the Map element
map.on('click', onMapClick);
